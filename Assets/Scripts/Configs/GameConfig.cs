using UnityEngine;

namespace ECSLiteTestTask.Configs
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "ECSLiteTestTask/Configs/Game")]
    public class GameConfig : Config
    {
        [SerializeField] private PlayerConfig _playerConfig;
        [SerializeField] private TargetConfig _targetConfig;
        
        public PlayerConfig PlayerConfig => _playerConfig;
        public TargetConfig TargetConfig => _targetConfig;
    }
}
