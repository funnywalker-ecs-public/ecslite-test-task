using UnityEngine;

namespace ECSLiteTestTask.Configs
{
    [CreateAssetMenu(fileName = "PlayerConfig", menuName = "ECSLiteTestTask/Configs/Player")]
    public class PlayerConfig : Config
    {
        [SerializeField] private GameObject _prefab;

        public GameObject Prefab => _prefab;
    }
}
