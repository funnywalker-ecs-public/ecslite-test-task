using UnityEngine;

namespace ECSLiteTestTask.Views
{
    public class EntityView : MonoBehaviour
    {
        public int Entity { get; set; } = -1;
    }
}
