using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedSystems;

namespace ECSLiteTestTask
{
    public static class EcsSystemsExtensions
    {
        public static EcsSystems DelHereEvents(this EcsSystems ecsSystems, string worldName = null)
        {
            ecsSystems.DelHere<MouseInputEvent>(worldName);
            ecsSystems.DelHere<UpdateNavMeshAgentTargetEvent>(worldName);
            ecsSystems.DelHere<ButtonPressedEvent>(worldName);
            ecsSystems.DelHere<FinishLevelEvent>(worldName);
            
            return ecsSystems;
        }
    }
}
