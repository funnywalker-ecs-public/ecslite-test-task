using ECSLiteTestTask.Common;
using ECSLiteTestTask.Configs;
using ECSLiteTestTask.Systems;
using LeoEcsPhysics;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace ECSLiteTestTask
{
    public class Startup : MonoBehaviour
    {
        [SerializeField] private GameConfig _gameConfig;
        [SerializeField] private SceneInfo _sceneInfo;
        
        private EcsWorld _ecsWorld;
        private EcsWorld _ecsWorldPhysics;
        private EcsWorld _ecsWorldEvents;

        private EcsSystems _ecsInitSystems;
        private EcsSystems _ecsUpdateSystems;
        private EcsSystems _ecsFixedUpdateSystems;
        
        private void Start()
        {
            _ecsWorld = new EcsWorld();
            _ecsWorldPhysics = new EcsWorld();
            _ecsWorldEvents = new EcsWorld();

            EcsPhysicsEvents.ecsWorld = _ecsWorldPhysics;

            _ecsInitSystems = new EcsSystems(_ecsWorld)
                .AddWorld(_ecsWorldEvents, GameConstants.EVENTS_WORLD)
                .Add(new SpawnPlayerSystem())
                .Add(new SpawnTargetSystem())
                .Add(new GatherEntitiesFromSceneSystem())
                .Add(new SetDoorAndButtonColorsSystem())
                .Inject(_gameConfig, _sceneInfo);
            
            _ecsUpdateSystems = new EcsSystems(_ecsWorld)
                .AddWorld(_ecsWorldPhysics, GameConstants.PHYSICS_WORLD)
                .AddWorld(_ecsWorldEvents, GameConstants.EVENTS_WORLD)
                .Add(new MouseInputSystem())
                .Add(new CameraMoveSystem())
                .Add(new DoorOpenTimerSystem())
                .Add(new UpdateTargetPositionSystem())
                .Add(new UpdateNavMeshAgentTargetSystem())
                .Add(new ProcessNavMeshAgentSystem())
                .Add(new UpdateAnimationSpeedParameterSystem())
                .Add(new ButtonTriggerEnterSystem())
                .Add(new PressButtonSystem())
                .Add(new OpenDoorSystem())
                .Add(new FinishTriggerEnterSystem())
                .Add(new FinishLevelSystem())
                .DelHerePhysics(GameConstants.PHYSICS_WORLD)
                .DelHereEvents(GameConstants.EVENTS_WORLD)
                .Inject(_gameConfig, _sceneInfo);
            
            _ecsFixedUpdateSystems = new EcsSystems(_ecsWorld)
                .Inject(_gameConfig, _sceneInfo);
            
#if UNITY_EDITOR
            _ecsUpdateSystems.Add(new Leopotam.EcsLite.UnityEditor.EcsWorldDebugSystem());
            _ecsUpdateSystems.Add(new Leopotam.EcsLite.UnityEditor.EcsWorldDebugSystem(GameConstants.PHYSICS_WORLD));
            _ecsUpdateSystems.Add(new Leopotam.EcsLite.UnityEditor.EcsWorldDebugSystem(GameConstants.EVENTS_WORLD));
            _ecsFixedUpdateSystems.Add(new Leopotam.EcsLite.UnityEditor.EcsWorldDebugSystem());
#endif
            
            _ecsInitSystems.Init();
            _ecsUpdateSystems.Init();
            _ecsFixedUpdateSystems.Init();
        }

        private void Update()
        {
            _ecsUpdateSystems.Run();
        }

        private void FixedUpdate()
        {
            _ecsFixedUpdateSystems.Run();
        }

        private void OnDestroy()
        {
            EcsPhysicsEvents.ecsWorld = null;
            
            DestroySystems(ref _ecsInitSystems);
            DestroySystems(ref _ecsUpdateSystems);
            DestroySystems(ref _ecsFixedUpdateSystems);
            
            DestroyWorld(ref _ecsWorld);
            DestroyWorld(ref _ecsWorldPhysics);
            DestroyWorld(ref _ecsWorldEvents);
        }

        private void DestroySystems(ref EcsSystems systems)
        {
            if (systems != null)
            {
                systems.Destroy();
                systems = null;
            }
        }

        private void DestroyWorld(ref EcsWorld world)
        {
            if (world != null)
            {
                world.Destroy();
                world = null;
            }
        }
    }
}
