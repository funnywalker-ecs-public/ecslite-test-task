using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    public class GameObjectLink : MonoBehaviour, IMonoComponentLink
    {
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ref var component = ref ecsWorld.GetPool<GameObjectComponent>().Add(entity);
            component.Value = gameObject;
            Destroy(this);
        }
    }
}
