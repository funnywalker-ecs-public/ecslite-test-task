using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    public class CameraTargetLink : MonoBehaviour, IMonoComponentLink
    {
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ecsWorld.GetPool<CameraTargetComponent>().Add(entity);
            Destroy(this);
        }
    }
}
