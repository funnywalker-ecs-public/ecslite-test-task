using Leopotam.EcsLite;

namespace ECSLiteTestTask.Links
{
    public interface IMonoComponentLink
    {
        public void Register(int entity, EcsWorld ecsWorld);
    }
}
