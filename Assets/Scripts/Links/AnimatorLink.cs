using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    [RequireComponent(typeof(Animator))]
    public class AnimatorLink : MonoBehaviour, IMonoComponentLink
    {
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ref var animatorComponent = ref ecsWorld.GetPool<AnimatorComponent>().Add(entity);
            animatorComponent.Value = GetComponent<Animator>();
            Destroy(this);
        }
    }
}
