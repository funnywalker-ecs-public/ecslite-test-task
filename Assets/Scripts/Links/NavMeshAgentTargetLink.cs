using Leopotam.EcsLite;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    public class NavMeshAgentTargetLink : MonoBehaviour, IMonoComponentLink
    {
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ref var component = ref ecsWorld.GetPool<Components.NavMeshAgentTargetComponent>().Add(entity);
            Destroy(this);
        }
    }
}
