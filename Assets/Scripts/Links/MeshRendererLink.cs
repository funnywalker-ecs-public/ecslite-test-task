using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    public class MeshRendererLink : MonoBehaviour, IMonoComponentLink
    {
        [SerializeField] private MeshRenderer _meshRenderer;
        
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ref var component = ref ecsWorld.GetPool<MeshRendererComponent>().Add(entity);
            component.Value = _meshRenderer;
            Destroy(this);
        }
    }
}
