using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using UnityEngine;
using UnityEngine.AI;

namespace ECSLiteTestTask.Links
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class NavMeshAgentLink : MonoBehaviour, IMonoComponentLink
    {
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ref var component = ref ecsWorld.GetPool<NavMeshAgentComponent>().Add(entity);
            component.Value = GetComponent<NavMeshAgent>();
            Destroy(this);
        }
    }
}
