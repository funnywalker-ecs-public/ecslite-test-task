using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    public class ColorLink : MonoBehaviour, IMonoComponentLink
    {
        [SerializeField] private Color _color = Color.white;
        
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ref var component = ref ecsWorld.GetPool<ColorComponent>().Add(entity);
            component.Value = _color;
            Destroy(this);
        }
    }
}
