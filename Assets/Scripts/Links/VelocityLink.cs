using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    public class VelocityLink : MonoBehaviour, IMonoComponentLink
    {
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ref var component = ref ecsWorld.GetPool<VelocityComponent>().Add(entity);
            component.CurrentVelocity = Vector3.zero;
            Destroy(this);
        }
    }
}
