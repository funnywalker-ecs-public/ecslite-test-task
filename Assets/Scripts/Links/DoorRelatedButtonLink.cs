using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using UnityEditor;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    public class DoorRelatedButtonLink : MonoBehaviour, IMonoComponentLink
    {
        [SerializeField] private GameObject _button;

        public void Register(int entity, EcsWorld ecsWorld)
        {
            ref var component = ref ecsWorld.GetPool<DoorRelatedButtonComponent>().Add(entity);
            component.Button = _button;
            Destroy(this);
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (_button != null)
            {
                var point1 = transform.position;
                var point2 = _button.transform.position;
                Handles.DrawBezier(point1, point2, point1, point2, Color.green, null, 5);
            }
        }
#endif
    }
}
