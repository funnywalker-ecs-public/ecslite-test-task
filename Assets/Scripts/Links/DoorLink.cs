using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    public class DoorLink : MonoBehaviour, IMonoComponentLink
    {
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ecsWorld.GetPool<DoorComponent>().Add(entity);
            Destroy(this);
        }
    }
}
