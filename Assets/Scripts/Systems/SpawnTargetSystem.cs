using ECSLiteTestTask.Configs;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace ECSLiteTestTask.Systems
{
    public class SpawnTargetSystem : IEcsInitSystem
    {
        private readonly EcsWorldInject _ecsWorld = default;
        
        private readonly EcsCustomInject<GameConfig> _gameConfig = default;
        private readonly EcsCustomInject<SceneInfo> _sceneInfo = default;

        public void Init(EcsSystems systems)
        {
            Object.Instantiate(_gameConfig.Value.TargetConfig.Prefab, _sceneInfo.Value.OffscreenSpawnPoint.position, Quaternion.identity);
        }
    }
}
