using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine.SceneManagement;

namespace ECSLiteTestTask.Systems
{
    public class FinishLevelSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<FinishLevelEvent>> _finishLevelFilter = GameConstants.EVENTS_WORLD;

        private readonly EcsPoolInject<FinishLevelEvent> _finishLevelPool = GameConstants.EVENTS_WORLD;

        public void Run(EcsSystems systems)
        {
            foreach (var finishLevelEntity in _finishLevelFilter.Value)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }
}
