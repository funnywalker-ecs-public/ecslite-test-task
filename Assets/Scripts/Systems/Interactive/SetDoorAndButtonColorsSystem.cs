using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class SetDoorAndButtonColorsSystem : IEcsInitSystem
    {
        private readonly EcsFilterInject<Inc<DoorRelatedButtonComponent, ColorComponent, MeshRendererComponent>> _doorFilter = default;
        private readonly EcsFilterInject<Inc<InteractableComponent, GameObjectComponent, MeshRendererComponent>> _buttonFilter = default;

        private readonly EcsPoolInject<DoorRelatedButtonComponent> _doorRelatedButtonPool = default;
        private readonly EcsPoolInject<ColorComponent> _colorPool = default;
        private readonly EcsPoolInject<MeshRendererComponent> _meshRendererPool = default;
        private readonly EcsPoolInject<GameObjectComponent> _gameObjectPool = default;

        public void Init(EcsSystems systems)
        {
            foreach (var entity in _doorFilter.Value)
            {
                ref var doorRelatedButtonComponent = ref _doorRelatedButtonPool.Value.Get(entity);
                ref var colorComponent = ref _colorPool.Value.Get(entity);
                ref var meshRendererComponent = ref _meshRendererPool.Value.Get(entity);

                meshRendererComponent.Value.material.color = colorComponent.Value;

                foreach (var buttonEntity in _buttonFilter.Value)
                {
                    ref var buttonGameObjectComponent = ref _gameObjectPool.Value.Get(buttonEntity);

                    if (buttonGameObjectComponent.Value.GetInstanceID() == doorRelatedButtonComponent.Button.GetInstanceID())
                    {
                        ref var buttonColorComponent = ref _colorPool.Value.Add(buttonEntity);
                        buttonColorComponent.Value = colorComponent.Value;

                        ref var buttonMeshRenderer = ref _meshRendererPool.Value.Get(buttonEntity);
                        buttonMeshRenderer.Value.material.color = colorComponent.Value;
                    }
                }
            }
        }
    }
}
