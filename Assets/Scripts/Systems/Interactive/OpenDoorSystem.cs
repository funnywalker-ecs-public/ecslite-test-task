using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class OpenDoorSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<ButtonPressedEvent>> _buttonPressedFilter = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<DoorComponent, DoorRelatedButtonComponent, AnimatorComponent>, Exc<OpenedDoorComponent>> _doorFilter = default;

        private readonly EcsPoolInject<ButtonPressedEvent> _buttonPressedPool = GameConstants.EVENTS_WORLD;
        
        private readonly EcsPoolInject<DoorRelatedButtonComponent> _doorRelatedButtonPool = default;
        private readonly EcsPoolInject<AnimatorComponent> _animatorPool = default;
        private readonly EcsPoolInject<OpenedDoorComponent> _openedDoorPool = default;
        private readonly EcsPoolInject<DoorOpenTimerComponent> _doorOpenTimerPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var buttonPressedEntity in _buttonPressedFilter.Value)
            {
                ref var buttonPressedEvent = ref _buttonPressedPool.Value.Get(buttonPressedEntity);
                
                foreach (var doorEntity in _doorFilter.Value)
                {
                    ref var doorRelatedButtonComponent = ref _doorRelatedButtonPool.Value.Get(doorEntity);
                    ref var doorAnimatorComponent = ref _animatorPool.Value.Get(doorEntity);

                    if (doorRelatedButtonComponent.Button.GetInstanceID() == buttonPressedEvent.Button.GetInstanceID())
                    {
                        _openedDoorPool.Value.Add(doorEntity);
                        
                        ref var doorOpenTimerComponent = ref _doorOpenTimerPool.Value.Add(doorEntity);
                        doorOpenTimerComponent.TimeRemaining = 1.0f;
                        
                        doorAnimatorComponent.Value.SetTrigger(GameConstants.TRIGGER_DOOR_OPEN);
                    }
                }
            }
        }
    }
}
