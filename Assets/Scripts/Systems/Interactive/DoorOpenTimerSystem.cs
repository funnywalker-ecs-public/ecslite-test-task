using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace ECSLiteTestTask.Systems
{
    public class DoorOpenTimerSystem : IEcsRunSystem
    {
        private readonly EcsWorldInject _ecsWorldEvents = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<DoorOpenTimerComponent>> _doorOpenTimerFilter = default;

        private readonly EcsPoolInject<DoorOpenTimerComponent> _doorOpenTimerPool = default;

        private readonly EcsPoolInject<UpdateNavMeshAgentTargetEvent> _updateTargetPool = GameConstants.EVENTS_WORLD;

        public void Run(EcsSystems systems)
        {
            foreach (var doorOpenTimerEntity in _doorOpenTimerFilter.Value)
            {
                ref var doorOpenTimerComponent = ref _doorOpenTimerPool.Value.Get(doorOpenTimerEntity);
                
                doorOpenTimerComponent.TimeRemaining -= Time.deltaTime;
                if (doorOpenTimerComponent.TimeRemaining <= 0.0f)
                {
                    _doorOpenTimerPool.Value.Del(doorOpenTimerEntity);
                    _updateTargetPool.Value.Add(_ecsWorldEvents.Value.NewEntity());
                }
            }
        }
    }
}
