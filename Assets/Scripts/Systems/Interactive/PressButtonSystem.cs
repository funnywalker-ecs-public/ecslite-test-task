using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class PressButtonSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<ButtonPressedEvent>> _buttonPressedFilter = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<ButtonComponent, GameObjectComponent, InteractableComponent, AnimatorComponent>> _interactableButtonFilter = default;
        
        private readonly EcsPoolInject<ButtonPressedEvent> _buttonPressedPool = GameConstants.EVENTS_WORLD;
        
        private readonly EcsPoolInject<InteractableComponent> _interactablePool = default;
        private readonly EcsPoolInject<AnimatorComponent> _animatorPool = default;
        private readonly EcsPoolInject<GameObjectComponent> _gameObjectPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var buttonPressedEntity in _buttonPressedFilter.Value)
            {
                ref var buttonPressedEvent = ref _buttonPressedPool.Value.Get(buttonPressedEntity);
                
                foreach (var interactableButtonEntity in _interactableButtonFilter.Value)
                {
                    ref var buttonGameObjectComponent = ref _gameObjectPool.Value.Get(interactableButtonEntity);
                    
                    if (buttonPressedEvent.Button.GetInstanceID() == buttonGameObjectComponent.Value.GetInstanceID())
                    {
                        _interactablePool.Value.Del(interactableButtonEntity);
                
                        ref var animatorComponent = ref _animatorPool.Value.Get(interactableButtonEntity);
                        animatorComponent.Value.SetTrigger(GameConstants.TRIGGER_BUTTON_PRESS);
                    }
                }
            }
        }
    }
}
