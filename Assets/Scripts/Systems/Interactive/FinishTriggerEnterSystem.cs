using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using LeoEcsPhysics;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class FinishTriggerEnterSystem : IEcsRunSystem
    {
        private readonly EcsWorldInject _ecsWorldEvents = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<OnTriggerEnterEvent>> _triggerEnterFilter = GameConstants.PHYSICS_WORLD;
        
        private readonly EcsFilterInject<Inc<GameObjectComponent, FinishComponent>> _finishFilter = default;
        
        private readonly EcsPoolInject<OnTriggerEnterEvent> _triggerEnterPool = GameConstants.PHYSICS_WORLD;
        
        private readonly EcsPoolInject<FinishLevelEvent> _finishLevelPool = GameConstants.EVENTS_WORLD;
        
        private readonly EcsPoolInject<GameObjectComponent> _gameObjectPool = default;
        
        public void Run(EcsSystems systems)
        {
            foreach (var entity in _triggerEnterFilter.Value)
            {
                ref var triggerEnterEvent = ref _triggerEnterPool.Value.Get(entity);

                foreach (var finishEntity in _finishFilter.Value)
                {
                    ref var gameObjectComponent = ref _gameObjectPool.Value.Get(finishEntity);
                    
                    if (gameObjectComponent.Value.GetInstanceID() == triggerEnterEvent.collider.gameObject.GetInstanceID())
                    {
                        _finishLevelPool.Value.Add(_ecsWorldEvents.Value.NewEntity());
                    }
                }
            }
        }
    }
}
