using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class UpdateAnimationSpeedParameterSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<AnimatorComponent, NavMeshAgentComponent>> _animatorFilter = default;

        private readonly EcsPoolInject<AnimatorComponent> _animatorPool = default;
        private readonly EcsPoolInject<NavMeshAgentComponent> _agentPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var animatorEntity in _animatorFilter.Value)
            {
                ref var animatorComponent = ref _animatorPool.Value.Get(animatorEntity);
                ref var agentComponent = ref _agentPool.Value.Get(animatorEntity);
                var currentSpeed = agentComponent.Value.velocity.magnitude;
                var maxSpeed = agentComponent.Value.speed;
                var normalizedSpeed = currentSpeed / maxSpeed;
                
                animatorComponent.Value.SetFloat(GameConstants.FLOAT_PLAYER_SPEED, normalizedSpeed);
            }
        }
    }
}
