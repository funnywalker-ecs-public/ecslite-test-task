using ECSLiteTestTask.Links;
using ECSLiteTestTask.Views;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace ECSLiteTestTask.Systems
{
    public class GatherEntitiesFromSceneSystem : IEcsInitSystem
    {
        private readonly EcsWorldInject _ecsWorld = default;
        
        public void Init(EcsSystems systems)
        {
            var entityViews = Object.FindObjectsOfType<EntityView>();
            foreach (var entityView in entityViews)
            {
                if (entityView.Entity < 0)
                {
                    var entity = _ecsWorld.Value.NewEntity();
                    entityView.Entity = entity;

                    var monoComponentLinks = entityView.GetComponents<IMonoComponentLink>();
                    foreach (var monoComponentLink in monoComponentLinks)
                    {
                        monoComponentLink.Register(entity, _ecsWorld.Value);
                    }
                }
            }
        }
    }
}
