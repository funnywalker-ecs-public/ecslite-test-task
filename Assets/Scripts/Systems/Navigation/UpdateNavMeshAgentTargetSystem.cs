using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using UnityEngine.AI;

namespace ECSLiteTestTask.Systems
{
    public class UpdateNavMeshAgentTargetSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<UpdateNavMeshAgentTargetEvent>> _updateTargetFilter = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<NavMeshAgentTargetComponent, GameObjectComponent, TransformComponent>> _targetFilter = default;
        private readonly EcsFilterInject<Inc<NavMeshAgentComponent>> _agentFilter = default;

        private readonly EcsPoolInject<NavMeshAgentComponent> _agentPool = default;
        private readonly EcsPoolInject<HasTarget> _hasTargetPool = default;
        private readonly EcsPoolInject<TransformComponent> _transformPool = default;
        private readonly EcsPoolInject<GameObjectComponent> _gameObjectPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var updateTargetEntity in _updateTargetFilter.Value)
            {
                foreach (var targetEntity in _targetFilter.Value)
                {
                    ref var targetTransformComponent = ref _transformPool.Value.Get(targetEntity);

                    foreach (var agentEntity in _agentFilter.Value)
                    {
                        ref var agentComponent = ref _agentPool.Value.Get(agentEntity);
                        var path = new NavMeshPath();
                        if (agentComponent.Value.CalculatePath(targetTransformComponent.Value.position, path))
                        {
                            agentComponent.Value.SetPath(path);
                            if (!_hasTargetPool.Value.Has(agentEntity))
                            {
                                ref var targetGameObjectComponent = ref _gameObjectPool.Value.Get(targetEntity);
                                targetGameObjectComponent.Value.SetActive(true);
                            
                                _hasTargetPool.Value.Add(agentEntity);
                            }
                        }
                    }
                }
            }
        }
    }
}
