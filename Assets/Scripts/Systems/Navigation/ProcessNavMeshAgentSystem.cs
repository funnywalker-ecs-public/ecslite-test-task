using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using UnityEngine.AI;

namespace ECSLiteTestTask.Systems
{
    public class ProcessNavMeshAgentSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<NavMeshAgentComponent, HasTarget>> _agentFilter = default;
        private readonly EcsFilterInject<Inc<NavMeshAgentTargetComponent, GameObjectComponent, TransformComponent>> _targetFilter = default;
    
        private readonly EcsPoolInject<NavMeshAgentComponent> _agentPool = default;
        private readonly EcsPoolInject<HasTarget> _hasTargetPool = default;
        private readonly EcsPoolInject<GameObjectComponent> _gameObjectPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var agentEntity in _agentFilter.Value)
            {
                ref var agentComponent = ref _agentPool.Value.Get(agentEntity);

                if (!agentComponent.Value.hasPath)
                {
                    _hasTargetPool.Value.Del(agentEntity);
                    
                    foreach (var targetEntity in _targetFilter.Value)
                    {
                        ref var targetGameObjectComponent = ref _gameObjectPool.Value.Get(targetEntity);
                        targetGameObjectComponent.Value.SetActive(false);
                    }
                }
            }
        }
    }
}
