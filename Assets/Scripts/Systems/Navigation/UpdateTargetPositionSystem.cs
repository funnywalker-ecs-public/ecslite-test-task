using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace ECSLiteTestTask.Systems
{
    public class UpdateTargetPositionSystem : IEcsRunSystem
    {
        private readonly EcsWorldInject _ecsWorldEvents = GameConstants.EVENTS_WORLD;
        
        private readonly EcsCustomInject<SceneInfo> _sceneInfo = default;

        private readonly EcsFilterInject<Inc<MouseInputEvent>> _mouseInputFilter = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<NavMeshAgentTargetComponent, TransformComponent>> _targetFilter = default;

        private readonly EcsPoolInject<MouseInputEvent> _mouseInputPool = GameConstants.EVENTS_WORLD;
        private readonly EcsPoolInject<UpdateNavMeshAgentTargetEvent> _updateTargetPool = GameConstants.EVENTS_WORLD;
        
        private readonly EcsPoolInject<TransformComponent> _transformPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var mouseInputEntity in _mouseInputFilter.Value)
            {
                ref var mouseInputEvent = ref _mouseInputPool.Value.Get(mouseInputEntity);
                var ray = _sceneInfo.Value.Camera.ScreenPointToRay(mouseInputEvent.Position);
                
                if (Physics.Raycast(ray, out var hit, 100.0f, 1 << 10))
                {
                    foreach (var targetEntity in _targetFilter.Value)
                    {
                        ref var transformComponent = ref _transformPool.Value.Get(targetEntity);
                        transformComponent.Value.position = hit.point;
                    
                        _updateTargetPool.Value.Add(_ecsWorldEvents.Value.NewEntity());
                    }
                }
            }
        }
    }
}
