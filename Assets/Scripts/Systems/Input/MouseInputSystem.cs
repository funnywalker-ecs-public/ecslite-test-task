using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace ECSLiteTestTask.Systems
{
    public class MouseInputSystem : IEcsRunSystem
    {
        private readonly EcsWorldInject _ecsWorldEvents = GameConstants.EVENTS_WORLD;
        
        private readonly EcsPoolInject<MouseInputEvent> _mouseInputPool = GameConstants.EVENTS_WORLD;

        public void Run(EcsSystems systems)
        {
            if (Input.GetMouseButtonUp(0))
            {
                ref var mouseInputEvent = ref _mouseInputPool.Value.Add(_ecsWorldEvents.Value.NewEntity());
                mouseInputEvent.Position = Input.mousePosition;
            }
        }
    }
}
