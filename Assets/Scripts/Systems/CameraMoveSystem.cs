using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace ECSLiteTestTask.Systems
{
    public class CameraMoveSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<CameraRootComponent, VelocityComponent, TransformComponent>> _cameraRootFilter = default;
        private readonly EcsFilterInject<Inc<CameraTargetComponent, TransformComponent>> _targetFilter = default;

        private readonly EcsPoolInject<TransformComponent> _transformPool = default;
        private readonly EcsPoolInject<VelocityComponent> _velocityPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var cameraRootEntity in _cameraRootFilter.Value)
            {
                ref var cameraRootTransformComponent = ref _transformPool.Value.Get(cameraRootEntity);
                ref var cameraRootVelocityComponent = ref _velocityPool.Value.Get(cameraRootEntity);
                
                foreach (var targetEntity in _targetFilter.Value)
                {
                    ref var targetTransformComponent = ref _transformPool.Value.Get(targetEntity);
                    var currentPosition = cameraRootTransformComponent.Value.position;
                    var targetPosition = targetTransformComponent.Value.position;
                    
                    var newPosition = Vector3.SmoothDamp(currentPosition, targetPosition, ref cameraRootVelocityComponent.CurrentVelocity, 0.3f);
                    
                    cameraRootTransformComponent.Value.position = newPosition;
                }
            }
            
        }
    }
}
