using ECSLiteTestTask.Configs;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace ECSLiteTestTask.Systems
{
    public class SpawnPlayerSystem : IEcsInitSystem
    {
        private readonly EcsWorldInject _ecsWorld = default;
        
        private readonly EcsCustomInject<GameConfig> _gameConfig = default;
        private readonly EcsCustomInject<SceneInfo> _sceneInfo = default;

        public void Init(EcsSystems systems)
        {
            Object.Instantiate(_gameConfig.Value.PlayerConfig.Prefab, _sceneInfo.Value.PlayerSpawnPoint.position, Quaternion.identity);
        }
    }
}
