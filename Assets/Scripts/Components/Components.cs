using UnityEngine;
using UnityEngine.AI;

namespace ECSLiteTestTask.Components
{
    #region Base Components

    public struct GameObjectComponent
    {
        public GameObject Value;
    }
    
    public struct TransformComponent
    {
        public Transform Value;
    }
    
    public struct NavMeshAgentComponent
    {
        public NavMeshAgent Value;
    }

    public struct AnimatorComponent
    {
        public Animator Value;
    }

    public struct MeshRendererComponent
    {
        public MeshRenderer Value;
    }
    
    public struct ColorComponent
    {
        public Color Value;
    }

    #endregion

    #region Input Events

    public struct MouseInputEvent
    {
        public Vector2 Position;
    }

    #endregion

    #region Navigation

    public struct NavMeshAgentTargetComponent { }
    
    public struct HasTarget { }

    #endregion

    #region Navigation Events

    public struct UpdateNavMeshAgentTargetEvent { }

    #endregion
    
    public struct PlayerComponent { }
    
    public struct ButtonComponent { }
    
    public struct DoorComponent { }

    public struct OpenedDoorComponent { }
    
    public struct InteractableComponent { }
    
    public struct CameraTargetComponent { }

    public struct DoorRelatedButtonComponent
    {
        public GameObject Button;
    }
    
    public struct DoorOpenTimerComponent
    {
        public float TimeRemaining;
    }
    
    public struct FinishComponent { }

    public struct VelocityComponent
    {
        public Vector3 CurrentVelocity;
    }
    
    public struct CameraRootComponent { }
    
    #region Events

    public struct ButtonPressedEvent
    {
        public GameObject Button;
    }
    
    public struct FinishLevelEvent { }

    #endregion
}
