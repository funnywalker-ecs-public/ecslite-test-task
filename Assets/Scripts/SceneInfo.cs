using UnityEngine;

namespace ECSLiteTestTask
{
    public class SceneInfo : MonoBehaviour
    {
        [SerializeField] private Camera _camera;
        [SerializeField] private Transform _playerSpawnPoint;
        [SerializeField] private Transform _offscreenSpawnPoint;

        public Camera Camera => _camera;
        public Transform PlayerSpawnPoint => _playerSpawnPoint;
        public Transform OffscreenSpawnPoint => _offscreenSpawnPoint;
    }
}
