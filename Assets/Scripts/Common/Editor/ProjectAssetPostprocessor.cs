#if UNITY_EDITOR
using System.Reflection;
using ECSLiteTestTask.Configs;
using UnityEditor;

namespace ECSLiteTestTask.EditorUtils
{
    public class ProjectAssetPostprocessor : AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            foreach (var importedAsset in importedAssets)
            {
                var asset = (Config)AssetDatabase.LoadAssetAtPath(importedAsset, typeof(Config));
                if (asset)
                {
                    var baseType = asset.GetType().BaseType;
                    if (baseType != null)
                    {
                        var idField = baseType.GetField("_id", BindingFlags.NonPublic | BindingFlags.Instance);
                        if (idField != null)
                        {
                            var oldValue = (string)idField.GetValue(asset);
                            var name = asset.name;
                            for (int i = 0; i < name.Length; i++)
                            {
                                if (i > 0 && char.IsUpper(name[i]))
                                {
                                    name = name.Insert(i, "_");
                                    i++;
                                }
                            }
                            var newValue = name.TrimStart().TrimEnd().ToLower().Replace(' ', '_');
                            idField.SetValue(asset, newValue);
                            if (oldValue != newValue)
                            {
                                EditorUtility.SetDirty(asset);
                                AssetDatabase.SaveAssetIfDirty(asset);
                            }
                        }
                    }
                }
            }
        }
    }
}
#endif
