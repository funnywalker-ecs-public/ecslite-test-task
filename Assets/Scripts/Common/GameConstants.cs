namespace ECSLiteTestTask.Common
{
    public class GameConstants
    {
        #region ECS

        public const string PHYSICS_WORLD = "physics";
        public const string EVENTS_WORLD = "events";

        #endregion

        #region Animations

        public const string TRIGGER_DOOR_OPEN = "Open";
        public const string TRIGGER_BUTTON_PRESS = "Press";
        public const string FLOAT_PLAYER_SPEED = "MoveSpeed";

        #endregion
    }
}
